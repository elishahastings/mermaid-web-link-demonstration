# Mermaid web link demonstration

This is an example project to demonstrate a mermaid bug in gitlab wikis.

The original issue is [here](https://gitlab.com/gitlab-org/gitlab-ce/issues/50459), and a demonstration of it is in this project's wiki, [here](https://gitlab.com/elishahastings/mermaid-web-link-demonstration/wikis/home)